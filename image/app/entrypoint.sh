#!/bin/sh -e

echo Converting...
rm -f $DESTDIR/index.html
mkdir -p $DESTDIR/person
$BASEDIR/bin/convert $SOLRURL $DESTDIR/person

echo Serving...
exec nginx -g 'daemon off;'
