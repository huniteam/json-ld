package REST::Solr;

use 5.20.0;
use warnings;

use Function::Parameters    qw( :strict );
use URI::Escape             qw( uri_escape );

use Moo;
extends qw( REST::Base );

# Returns doc|undef. Dies on multiple matches.
method find_docid($docid) {
    my $found = $self->_query($docid);

    return unless @$found; # quietly return nothing if none found
    return $found->[0] if @$found == 1;

    my @matches = sort map { $_->{docid} } @$found;
    my $matches = @matches;
    local $, = ', ';
    die "$docid matched $matches results: " . join(', ', @matches) . "\n";
}

# Returns a hash of docid => doc|undef.
method find_docids(@docids) {
    my $found = $self->_query(@docids);
    my %found = map { $_->{docid} => $_ } @$found;
    return { map { $_ => $found{$_} } @docids };
}

method _query(@docids) {
    # Using json body parameters in a post request allows many more docids
    # than do query parameters in a get.
    my $data = {
        params => {
            wt => 'json',
            q => join(' OR ', map { 'docid:' . escape($_) } @docids),
            rows => scalar @docids,
        },
    };
    my $result = $self->post('', data => $data);
    return $result->{response}->{docs};
}

fun escape($docid) {
    return $docid =~ s/\*/\\*/gr;
}

method fetch_by_type($entityType, $start, $count) {
    my $data = {
        params => {
            wt => 'json',
            q => 'entityType:' . $entityType,
            start => $start,
            rows => $count,
        },
    };
    my $result = $self->post('', data => $data);
    return $result->{response}->{docs};
}

1;
