package REST::Base;

use 5.20.0;
use warnings;

use Cpanel::JSON::XS        qw( );
use Data::Dumper::Concise   qw( Dumper );
use Function::Parameters    qw( :strict );
use Furl                    qw( );

use Moo;

has base_url => (
    is => 'ro',
    required => 1,
);

has default_headers => (
    is => 'lazy',
    builder => sub { [
        'Accept'            => 'application/json;charset=utf-8',
        'Accept-Encoding'   => 'gzip, deflate',
        'Connection'        => 'close', # this avoids the wacky CONNRESET stuff
        'Content-Type'      => 'application/json;charset=utf-8',
    ] },
);

has headers => (
    is => 'lazy',
    builder => sub { [ ] },
);

has furl => (
    is => 'lazy',
    builder => method() {
        Furl->new(
            headers => [
                @{ $self->default_headers },
                @{ $self->headers },
            ],
        )
    },
);

has json => (
    is => 'lazy',
    builder => method() {
        Cpanel::JSON::XS->new->utf8,
    },
);

method request($method, $path, :$data = undef, :$headers = undef) {
    $data = $self->json->encode($data) if defined $data;
    my $url = $self->url($path);
    my $request = Furl::Request->new($method, $url, $headers, $data);
    my $response = $self->furl->request($request);

    die Dumper($response) unless $response->is_success;

    return $self->json->decode($response->decoded_content);
}

method url($path) {
    return $self->base_url . $path;
}

sub delete { shift->request(DELETE => @_) }
sub head   { shift->request(HEAD   => @_) }
sub get    { shift->request(GET    => @_) }
sub post   { shift->request(POST   => @_) }
sub put    { shift->request(PUT    => @_) }

1;
