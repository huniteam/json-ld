package JSONLD::Convert;

use 5.20.0;
use warnings;

use Exporter                qw( import );
use Function::Parameters    qw( :strict );

our @EXPORT_OK = qw( convert_person );

# http://schema.org/Person

my $huni_base = 'https://huni.net.au/#/record/';

my %fields = (
    # json-ld       solr
    birthDate       => [ 'birthDate', \&convert_date ],
    deathDate       => [ 'deathDate', \&convert_date ],
    description     => [ 'biography', \&trim ],
    familyName      => 'familyName',
    givenName       => 'individualName',
    nationality     => 'ethnicity',
    url             => [ 'docid', \&convert_url ],
);

fun convert_person($solr) {
    my $json = {
        '@context'  => 'http://schema.org/',
        '@type'     => 'Person',
    };

    my $noop = sub { $_[0] };

    while (my ($json_key, $solr_key) = each %fields) {
        my $transform = $noop;
        if (ref $solr_key) {
            ($solr_key, $transform) = @$solr_key;
        }

        my $value = $solr->{$solr_key};
        if (defined $value) {
            $json->{$json_key} = $transform->($value);
        }
    };

    return $json;
}

fun convert_date($solrDate) {
    $solrDate =~ /^(\d{4}-\d{2}-\d{2})/ && $1;
}

fun trim($str) {
    $str =~ s/^\s*|\s*$//gr;
}

fun convert_url($docid) {
    $huni_base . $docid
}

1;
