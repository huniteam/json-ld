# HuNI JSON-LD Service

This docker container contains the complete set of HuNI person records as
JSON-LD using the http://schema.org/Person schema.

When the container starts up, the records are fetched from HuNI and converted to
JSON-LD. Once the documents are ready the nginx server is started on port 80.
